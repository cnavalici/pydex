PyDEX
==============

    
    

What's PyDEX?
---------------


PyDEX is a opensource desktop application, which works with
all romanian dictionaries offered by [dexonline.ro](http://www.dexonline.ro)

The applications can also update the dictionaries directly from the dexonline website,
to be always up-to-date with the words definitions.


For this moment, the UI is only in romanian. It might be translated in the next versions.



Requirements
---------------

PyDEX is a python application, so it can be run under multiple operating systems (Linux, Windows, MacOS):


    python >= 2.7 (with sqlite3 included support; sqlite3 >= 3.7.4)
    wxPython >= 2.8
    python-lxml module




How to get it?
---------------

Use git's feature to generate an archive:

[Git Master Zip](https://github.com/cristianav/PyDEX/archive/master.zip)


or go to the official website and download the latest stable version:


[Pydex Website](http://pydex.lemonsoftware.eu/en/download)


Note that the full database is available only from the pydex website.




How to install it
---------------

Pretty straightforward:


+ Download/unpack pydex archive using one of the methods above
+ Download/unpack database (7z) in the *share/data/* folder (optional) 
+ PyDEX is run by *bin/pydex*

If you don't unpack the provided database, then you will need to do a full update in the first place.
