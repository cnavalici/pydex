# -*- coding: utf-8 -*-
#
#       helpers.py
#
#       Copyright Cristian Navalici ncristian [at]  lemonsoftware.eu
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import re
from datetime import datetime

def convert_term_correct_utf8(term_to_convert):
    '''şţŞŢ \u015f\u0163\u015e\u0162 - utf8
       șțȘȚ \u0219\u021b\u0218\u021a  - utf8'''

    return term_to_convert.replace(u'\u015f',u'\u0219') \
                    .replace(u'\u0163',u'\u021b') \
                    .replace(u'\u015e',u'\u0218') \
                    .replace(u'\u0162',u'\u021a') \


class SearchManager(object):
    history_search = []

    def notify_search(self, searched_term):
        if not searched_term in self.history_search:
            self.history_search.append(searched_term)



def html_format_source(string, lastupd_date):
    '''prepare the "Source" string to look like html'''
    lu = datetime.fromtimestamp(float(lastupd_date)).strftime("%d-%m-%Y %H:%M:%S")
    return "<i><font color='green'>Sursa: {} Ultimul update: {}</font></i><br>".format(string, lu)


def html_format_string(string):
    '''prepare the string to look like html
    some custom dexonline formatting: between $-$ => italic, @-@ bold, %-% more space
    '''
    bold_pos_list = [c.start() for c in re.finditer('@', string)]
    italic_pos_list = [c.start() for c in re.finditer('\$', string)]
    abbrev_pos_list = [c.start() for c in re.finditer('#', string)]

    lstring = list(string)

    # changing @ to <b> or </b>
    for i in range(0, len(bold_pos_list), 2): lstring[bold_pos_list[i]] = '<b>'
    for i in range(1, len(bold_pos_list), 2): lstring[bold_pos_list[i]] = '</b>'

    # changing $ to <i> or </i>
    for i in range(0, len(italic_pos_list), 2): lstring[italic_pos_list[i]] = '<i>'
    for i in range(1, len(italic_pos_list), 2): lstring[italic_pos_list[i]] = '</i>'

    for i in range(0, len(abbrev_pos_list), 2): lstring[abbrev_pos_list[i]] = '<font color="#809efe">'
    for i in range(1, len(abbrev_pos_list), 2): lstring[abbrev_pos_list[i]] = '</font>'

    lstring.append('<br>')
    return "".join(lstring)
