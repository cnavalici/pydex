# -*- coding: utf-8 -*-
#
#       main_window.py
#
#       Copyright Cristian Navalici ncristian [at]  lemonsoftware.eu
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import wx
import wx.html
import os

from StringIO import StringIO
from wx.html import HtmlEasyPrinting

from pydex import TMP_FOLDER, ICONFILE, PYDEX_VERSION, PYDEX_NAME, ABOUT_DICT
from controller import Controller
from helpers import convert_term_correct_utf8


class DexFrame(wx.Frame):
    def __init__(self, parent):
        title_str = "PyDEX - Dexonline ver {}".format(PYDEX_VERSION)
        wx.Frame.__init__(self, parent, title=title_str, size=(800, 600))
        favicon = wx.Icon(ICONFILE, wx.BITMAP_TYPE_ICO, 16, 16)
        self.SetIcon(favicon)

        if not os.path.exists(TMP_FOLDER): os.mkdir(TMP_FOLDER)

        self.controller = Controller(self)

        self.__init_create_status_bar()
        self.__init_create_top_menu()
        self.__init_create_window_structure()

        import sys
        reload(sys)
        sys.setdefaultencoding("UTF-8")
        self.pob = HtmlEasyPrinting()
        self.pob.encoding = "UTF-8"

        self.update_statusbar_lastupdate()

        self.Centre()
        self.Show(True)


    def __init_create_status_bar(self):
        self.statusbar = self.CreateStatusBar()
        self.statusbar.SetFieldsCount(2)
        self.statusbar.SetStatusWidths([-1, 270])


    def __init_create_top_menu(self):
        filemenu = wx.Menu()
        menuUpdate = filemenu.Append(wx.ID_OPEN, "Actualizează DB", "Actualizează baza de date")
        menuExit = filemenu.Append(wx.ID_EXIT, "Ieşire", " Termină programul")

        aboutmenu = wx.Menu()
        menuAbout = aboutmenu.Append(wx.ID_ABOUT, "Despre", "Informaţii despre acest program")
        menuHelp = aboutmenu.Append(wx.ID_HELP, "Ajutor", "Manualul de instrucţiuni")
        menuHelp.Enable(False) # temporary until we'll have some kind of help

        viewmenu = wx.Menu()
        menuSources = viewmenu.Append(wx.NewId(), "Surse", "Surse")
        menuAbbrevs = viewmenu.Append(wx.NewId(), "Abrevieri", "Abrevieri")

        self.Bind(wx.EVT_MENU, self.on_quit, menuExit)
        self.Bind(wx.EVT_MENU, self.on_about, menuAbout)
        self.Bind(wx.EVT_MENU, self.on_help, menuHelp)
        self.Bind(wx.EVT_MENU, self.on_update, menuUpdate)
        self.Bind(wx.EVT_MENU, self.on_sources, menuSources)
        self.Bind(wx.EVT_MENU, self.on_abbrevs, menuAbbrevs)

        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "Fişier")
        menuBar.Append(viewmenu, "Vizualizare")
        menuBar.Append(aboutmenu, "Ajutor")
        self.SetMenuBar(menuBar)


    def __init_create_window_structure(self):
        panel = wx.Panel(self, -1)
        panel.SetBackgroundColour(wx.NullColor)
        vbox = wx.BoxSizer(wx.VERTICAL)

        # first horizontal box - search area
        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        st_search = wx.StaticText(panel, -1, 'Termen:')
        cmb_search = wx.ComboBox(panel, -1, style=wx.CB_DROPDOWN,  size=wx.Size(300, -1))
        self.Bind(wx.EVT_TEXT_ENTER,  self.__do_search_cb,  cmb_search)

        btn_print_preview = wx.Button(panel,  id=wx.ID_PREVIEW)
        self.Bind(wx.EVT_BUTTON, self.on_print_preview, btn_print_preview)

        btn_print = wx.Button(panel, id=wx.ID_PRINT)
        self.Bind(wx.EVT_BUTTON, self.on_print, btn_print)

        cmb_source = wx.ComboBox(panel, -1, choices=self.__populate_sources_cmb(), style=wx.CB_READONLY,  pos=(1,1))
        cmb_source.SetSelection(0)

        hbox1.Add(st_search, 0, wx.LEFT, 8) # (item, proportion, flag, border, userData)
        hbox1.Add(cmb_search, 0, wx.LEFT)
        hbox1.Add(cmb_source, 0, wx.RIGHT)
        hbox1.Add(btn_print_preview, 0, wx.RIGHT)
        hbox1.Add(btn_print, 0, wx.RIGHT)
        vbox.Add(hbox1, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)

        # second horizontal - options
        hbox2 = wx.BoxSizer(wx.HORIZONTAL)

        self.cbx_with_diacritics = wx.CheckBox(panel, label="Numai cu Diacritice")
        self.cbx_exact_search = wx.CheckBox(panel, label="Căutare exactă")
        self.cbx_exact_search.SetValue(1)
        self.cbx_complete_definitions = wx.CheckBox(panel, label="Căutare Completă Definiţii")

        checkboxes_group = [self.cbx_with_diacritics, self.cbx_exact_search, self.cbx_complete_definitions]

        for checkbox in checkboxes_group:
            self.Bind(wx.EVT_CHECKBOX, self.__set_cbx_options, checkbox)

        hbox2.AddMany(checkboxes_group)
        vbox.Add(hbox2, 0, wx.LEFT | wx.RIGHT,   10)

        # third horizontal - text area
        hbox3 = wx.BoxSizer(wx.HORIZONTAL)

        txt_main = wx.html.HtmlWindow(panel, wx.ID_ANY, style=wx.NO_BORDER|wx.html.HW_SCROLLBAR_AUTO)

        hbox3.Add(txt_main, 1, wx.EXPAND)
        vbox.Add(hbox3, 1, wx.LEFT | wx.RIGHT | wx.EXPAND, 10)

        panel.SetSizer(vbox)

        self.cmbsearch = cmb_search
        self.cmbsource = cmb_source
        self.txtmain = txt_main


    def on_print(self, ev):
        self.pob.PrintText(self.controller.string_display)

    def on_print_preview(self, ev):
        self.pob.PreviewText(self.controller.string_display)


    def on_sources(self, ev): self.controller.show_sources(self)

    def on_abbrevs(self, ev): self.controller.show_abbrevs(self)

    def on_update(self, ev): self.controller.show_update(self)

    def on_quit(self, e): self.Close(True)

    def on_about(self, e):
        info = wx.AboutDialogInfo()

        info.SetIcon(wx.Icon(ICONFILE, wx.BITMAP_TYPE_ICO))
        info.SetName(PYDEX_NAME)
        info.SetVersion(PYDEX_VERSION)
        info.SetDescription(ABOUT_DICT['description'])
        info.SetCopyright(ABOUT_DICT['copyright'])
        info.SetWebSite(ABOUT_DICT['website'])
        info.SetLicence(ABOUT_DICT['licence'])
        info.AddDeveloper(ABOUT_DICT['developer'])
        info.AddDocWriter(ABOUT_DICT['docwriter'])

        wx.AboutBox(info)


    def on_help(self, e): pass


    def update_statusbar_lastupdate(self):
        '''display in status bar the last update'''
        lastupdate = self.controller.get_db_last_update()
        self.statusbar.SetStatusText("Ultimul update: {}".format(lastupdate), 1)


    def update_statusbar_results(self, results = 0):
        '''display in status bar the number of results'''
        self.statusbar.SetStatusText("Rezultate: {:>3}".format(results), 0)


    def __populate_sources_cmb(self):
        '''populates the sources combobox'''
        return self.controller.populate_dictionaries_sources()


    def __populate_search_history_cmb(self):
        '''populates the search combo history'''
        self.cmbsearch.Clear()
        self.cmbsearch.AppendItems(self.controller.get_search_history())
        #self.cmbsearch.SetSelection(0)


    def __do_search_cb(self,  evt):
        '''do search when press Enter in the combo'''
        search_term = convert_term_correct_utf8(evt.GetString().strip())
        if not search_term:
            dlg = wx.MessageDialog(self, 'Specificaţi un termen de căutat.', 'Atenţie',  wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            return 0

        # search (lexems or definitions)
        search_options = {
            'search_term': search_term,
            'source_id':  self.cmbsource.GetCurrentSelection(),
            'search_type': 'definitions' if self.cbx_complete_definitions.IsChecked() else 'lexems',
            'options': { 'diacritics': self.cbx_with_diacritics.IsChecked(), 'exact': self.cbx_exact_search.IsChecked() }
        }

        self.controller.do_search(search_options)
        self.controller.update_results_text_area()

        self.__populate_search_history_cmb()


    def __set_cbx_options(self,  event):
        if self.cbx_complete_definitions.IsChecked():
            self.cbx_with_diacritics.Disable()
            self.cbx_exact_search.Disable()
        else:
            self.cbx_with_diacritics.Enable()
            self.cbx_exact_search.Enable()
