# -*- coding: utf-8 -*-
#
#       settings.py
#
#       Copyright Cristian Navalici ncristian [at]  lemonsoftware.eu
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.


from db_utils import DBUtils

class SettingsDB(object):
    """this class works directly with the db settings table"""

    def __init__(self): self.refresh_settings()

    def get_update_link(self):
        # TODO: implement here a simple checking for link format
        return [ setting[2] for setting in self.settings if setting[1] == 'update_link' ][0]

    def get_lastupdate(self, forceupdate = 0):
        if forceupdate: self.refresh_settings()
        return [ setting[2] for setting in self.settings if setting[1] == 'lastupdate' ][0]

    def refresh_settings(self):
        dbob = DBUtils()
        self.settings = dbob.get_settings()
