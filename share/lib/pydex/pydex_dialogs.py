# -*- coding: utf-8 -*-
#
#       pydex_dialogs.py
#
#       Copyright Cristian Navalici ncristian [at]  lemonsoftware.eu
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.


import Queue

from threading import Thread
from urllib import urlencode
from urllib2 import HTTPError, Request, URLError, urlopen

import wx
import wx.grid as gridlib

TextEventType = wx.NewEventType()
EVT_THREAD_TEXT_EVENT = wx.PyEventBinder(TextEventType, 1)

import controller


# -------------------------------------------------------------------------------------------------
# DB UPDATE DIALOG
# -------------------------------------------------------------------------------------------------
class DbUpdateDlg(wx.Dialog):
    """update the database"""

    def __init__(self, parent=None):
        wx.Dialog.__init__(self, parent, title="Actualizează baza de date", size=(500, 200))
        ##self.DBob = dbutils.DBUtils()
        self.Bind(EVT_THREAD_TEXT_EVENT, self.__update_txt_message)

        # get last timestamp to check if a major update is needed
        self.cob = controller.Controller(self)
        lastupdate = self.cob.get_db_last_update()

        if not lastupdate:
            text = "PyDEX a detectat ca baza de date nu a fost niciodata actualizată. Un update major este necesar.\n"
            text += "În funcţie de performanţele computerului, acest lucru va dura câteva minute bune. "
            text += "Dacă nu doriţi actualizare directă, descărcaţi baza de date deja existentă "
            text += "de pe siteul oficial (urmăriţi manualul pt detalii). \n"
            text += "Continuaţi cu actualizare?"
        else:
            text = "Ultimul update: %s" % lastupdate

        panel = wx.Panel(self, -1)
        vbox = wx.BoxSizer(wx.VERTICAL)

        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.txt_updatemsg = wx.StaticText(panel, -1, label=text)
        hbox1.Add(self.txt_updatemsg, -1, wx.LEFT, 8)
        vbox.Add(hbox1, 1, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.gauge = wx.Gauge(panel, -1, style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)
        hbox2.Add(self.gauge, -1, wx.LEFT)
        vbox.Add(hbox2, 1, wx.EXPAND | wx.TOP, 50)

        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.btn_update = wx.Button(panel, id=wx.NewId(), label="Actualizează")
        self.btn_cancel = wx.Button(panel, id=wx.ID_CANCEL)

        self.Bind(wx.EVT_BUTTON, self.do_update, self.btn_update)
        self.Bind(wx.EVT_BUTTON, self.on_quit, self.btn_cancel)

        hbox3.Add(self.btn_update, 0, wx.RIGHT)
        hbox3.Add(self.btn_cancel, 0, wx.RIGHT)
        vbox.Add(hbox3, 0, wx.EXPAND | wx.ALL, 10)

        panel.SetSizer(vbox)



    def do_update(self, e):
        self.txt_updatemsg.SetLabel("Nu întrerupeţi această operaţie \n")
        self.btn_update.Disable()
        self.btn_cancel.Disable()

        try:
            thread = Thread(target=self.start_update_thread)
            thread.start()
            #thread.join() # don't do it
            while thread.is_alive():
                wx.Yield()
                wx.MilliSleep(200)
                self.gauge.Pulse()
            else:
                self.end_update_thread()
        except Exception, e:
            print "do_update", e



    def start_update_thread(self):
        self.result_update = self.cob.do_db_update(self)
        #wx.CallAfter(self.end_update_thread())


    def end_update_thread(self):
        try:
            if self.result_update: msg_str = "Actualizarea a luat sfârşit."
            else: msg_str = "Actualizarea a eşuat!"

            wx.CallAfter(self.txt_updatemsg.SetLabel, msg_str)

            self.btn_cancel.Enable()
            self.gauge.Hide()
        except Exception, e:
            print "end_update_thread", e


    def __update_txt_message(self, evt): self.txt_updatemsg.SetLabel(evt.getText())

    def  on_quit(self, e): self.Destroy()



# -------------------------------------------------------------------------------------------------
# SETTINGS DIALOG
# -------------------------------------------------------------------------------------------------
class SettingsDlg(wx.Dialog):
    """settings dialog"""
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, title="Setări", size=(600, 400))
        panel = wx.Panel(self, -1)
        vbox = wx.BoxSizer(wx.VERTICAL)

        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        st_credentials = wx.StaticText(panel, -1, 'Date de conectare pe dexonline.ro (creaţi-vă mai întâi un cont pe site)')
        hbox1.Add(st_credentials, 1, wx.LEFT, 8)
        vbox.Add(hbox1, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        st_username = wx.StaticText(panel, 0, 'Utilizator:')
        txt_username = wx.TextCtrl(panel, size=(120, -1))
        st_password = wx.StaticText(panel, 0, 'Parolă:')
        txt_password = wx.TextCtrl(panel, size=(120, -1), style=wx.TE_PASSWORD)
        btn_check_cred = wx.Button(panel, id=wx.NewId(), label="Verifică datele")
        self.Bind(wx.EVT_BUTTON, self.check_credentials_onsite, btn_check_cred)

        hbox2.Add(st_username, 0, wx.LEFT, 8)
        hbox2.Add(txt_username, 0, wx.LEFT, 8)
        hbox2.Add(st_password, 0, wx.LEFT, 8)
        hbox2.Add(txt_password, 0, wx.LEFT, 8)
        hbox2.Add(btn_check_cred, 0, wx.LEFT, 8)
        vbox.Add(hbox2, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)

        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        st_result_credential = wx.StaticText(panel, wx.ID_ANY, label="", style=wx.ALIGN_CENTER)
        hbox3.Add(st_result_credential, 1, wx.LEFT)
        vbox.Add(hbox3, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)

        panel.SetSizer(vbox)

        # use these globally
        self.txt_username = txt_username
        self.txt_password = txt_password
        self.result_credential = st_result_credential


    def check_credentials_onsite(self, ev):
        """checks the entered credentials on dexonline.ro website"""
        try:
            req = Request("http://dexonline.ro/login")
            data = urlencode([('password', self.txt_password.GetValue()), ('email', self.txt_username.GetValue()), ('login', 'Conectare')])
            response = urlopen(req, data)
            #print response.geturl() # debug
            #print response.info() # debug
            login = response.read().find('flashMessage errorType') # look for this string in the page, might be changed over time!
            if login != -1:
                self.result_credential.SetLabel("Logarea a eşuat. Verificaţi datele de access.")
            else:
                self.result_credential.SetLabel("Logarea s-a realizat cu success.")
        except HTTPError, e:
            dlg = wx.MessageDialog(self, 'Serverul nu a putut îndeplini cererea. Cod eroare: %s' % e.code, "Eroare", wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()
        except URLError, e:
            dlg = wx.MessageDialog(self, 'Nu reuşim să contactăm serverul. Motiv: %s' % e.reason, "Eroare", wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()
        except Exception, e:
            dlg = wx.MessageDialog(self, 'Eroare: %s' % e, "Eroare", wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()


# -------------------------------------------------------------------------------------------------
# CUSTOM EVENT - used to communicate from Thread to Main GUI Window
# -------------------------------------------------------------------------------------------------
class NewTextEvent(wx.PyCommandEvent):
    def __init__(self, evtType, id):
        wx.PyCommandEvent.__init__(self, evtType, id)
        self.msg = ''

    def setText(self, text):
        self.msg = text

    def getText(self):
        return self.msg



# -------------------------------------------------------------------------------------------------
# SOURCES (ACRONIMS) DIALOG
# -------------------------------------------------------------------------------------------------
class SourcesDlg(wx.Dialog):
    """acronims dialog"""
    def __init__(self, parent, sources):
        wx.Dialog.__init__(self, parent, title="Acronime", size=(1000, 800),
                           style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        panel = wx.Panel(self, -1, style=0)
        grid =  SourcesTableGrid(panel, sources)

        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(grid, 1, wx.GROW|wx.ALL, 5)
        panel.SetSizer(bs)

    def  on_quit(self, e):
        self.Destroy()


class SourcesTableGrid(gridlib.Grid):
    def __init__(self, parent, tbldata):
        gridlib.Grid.__init__(self, parent, -1)

        table = SourcesTable(tbldata)

        # The second parameter means that the grid is to take ownership of the
        # table and will destroy it when done.  Otherwise you would need to keep
        # a reference to it and call it's Destroy method later.
        self.SetTable(table, True)

        self.SetRowLabelSize(0)
        self.SetMargins(0,0)
        self.AutoSizeColumns(False)
        self.EnableEditing(False)


class SourcesTable(gridlib.PyGridTableBase):
    def __init__(self, tbldata):
        gridlib.PyGridTableBase.__init__(self)
        self.colLabels = ['Id', 'Acronim', 'Definiţie', 'Autor', 'Editura', 'An']

        self.dataTypes = [
                gridlib.GRID_VALUE_NUMBER,
                gridlib.GRID_VALUE_STRING,
                gridlib.GRID_VALUE_STRING,
                gridlib.GRID_VALUE_STRING,
                gridlib.GRID_VALUE_NUMBER ]

        self.data = tbldata

    #--------------------------------------------------
    # required methods for the wx.PyGridTableBase interface
    def GetNumberRows(self):
        return len(self.data) + 1

    def GetNumberCols(self):
        return len(self.data[0])

    def IsEmptyCell(self, row, col):
        try:
            return not self.data[row][col]
        except IndexError:
            return True

    # Get/Set values in the table.  The Python version of these
    # methods can handle any data-type, (as long as the Editor and
    # Renderer understands the type too,) not just strings as in the
    # C++ version.
    def GetValue(self, row, col):
        try:
            return self.data[row][col]
        except IndexError:
            return ''

    def SetValue(self, row, col, value):
        def innerSetValue(row, col, value):
            try:
                self.data[row][col] = value
            except IndexError:
            # add a new row
                self.data.append([''] * self.GetNumberCols())
                innerSetValue(row, col, value)

            # tell the grid we've added a row
            msg = gridlib.GridTableMessage(self,                # The table
                gridlib.GRIDTABLE_NOTIFY_ROWS_APPENDED,       # what we did to it
                1                                      # how many
            )

            self.GetView().ProcessTableMessage(msg)
        innerSetValue(row, col, value)

    # OPTIONALS METHODS
    # Called when the grid needs to display labels
    def GetColLabelValue(self, col):
        return self.colLabels[col]


# -------------------------------------------------------------------------------------------------
# ABBREVIATIONS DIALOG
# -------------------------------------------------------------------------------------------------
class AbbrevsDlg(wx.Dialog):
    '''abbreviations dialog'''
    def __init__(self, parent, abbrevs):
        wx.Dialog.__init__(self, parent, title="Abrevieri", size=(1000, 800),
                           style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        panel = wx.Panel(self, -1, style=0)

        grid = AbbrevsTableGrid(panel, abbrevs)

        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(grid, 1, wx.GROW|wx.ALL, 5)
        panel.SetSizer(bs)


    def  on_quit(self, e):
        self.Destroy()


class AbbrevsTableGrid(gridlib.Grid):
    def __init__(self, parent, tbldata):
        gridlib.Grid.__init__(self, parent, -1)

        table = AbrevsTable(tbldata)

        # The second parameter means that the grid is to take ownership of the
        # table and will destroy it when done.  Otherwise you would need to keep
        # a reference to it and call it's Destroy method later.
        self.SetTable(table, True)

        self.SetRowLabelSize(0)
        self.SetMargins(0,0)
        self.AutoSizeColumns(True)
        self.EnableEditing(False)


class AbrevsTable(gridlib.PyGridTableBase):
    def __init__(self, tbldata):
        gridlib.PyGridTableBase.__init__(self)
        self.colLabels = ['Numele secţiunii', 'Abreviere scurtă', 'Abreviere', 'Ambiguă?']

        self.dataTypes = [
                gridlib.GRID_VALUE_STRING,
                gridlib.GRID_VALUE_STRING,
                gridlib.GRID_VALUE_STRING,
                gridlib.GRID_VALUE_NUMBER ]

        self.data = tbldata


    #--------------------------------------------------
    # required methods for the wx.PyGridTableBase interface
    def GetNumberRows(self):
        return len(self.data) + 1

    def GetNumberCols(self):
        return len(self.data[0])

    def IsEmptyCell(self, row, col):
        try:
            return not self.data[row][col]
        except IndexError:
            return True

    # Get/Set values in the table.  The Python version of these
    # methods can handle any data-type, (as long as the Editor and
    # Renderer understands the type too,) not just strings as in the
    # C++ version.
    def GetValue(self, row, col):
        try:
            return self.data[row][col]
        except IndexError:
            return ''


    def SetValue(self, row, col, value):
        def innerSetValue(row, col, value):
            try:
                self.data[row][col] = value
            except IndexError:
            # add a new row
                self.data.append([''] * self.GetNumberCols())
                innerSetValue(row, col, value)

            # tell the grid we've added a row
            msg = gridlib.GridTableMessage(self,                # The table
                gridlib.GRIDTABLE_NOTIFY_ROWS_APPENDED,       # what we did to it
                1                                      # how many
            )

            self.GetView().ProcessTableMessage(msg)
        innerSetValue(row, col, value)

    # OPTIONALS METHODS
    # Called when the grid needs to display labels
    def GetColLabelValue(self, col):
        return self.colLabels[col]



#EOF
