# -*- coding: utf-8 -*-
#
#       db_utils.py
#
#       Copyright Cristian Navalici ncristian [at]  lemonsoftware.eu
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import sys
import os
import wx
import unicodedata
import time
import sqlite3

from datetime import datetime
from abc import abstractmethod


from pydex import DBFILE, DBPATH



class DBBase(object):
    conn = None
    cursor = None

    def __init__(self):
        firsttime_flag = False

        if not os.path.exists(DBPATH):
            os.mkdir(DBPATH)

        if not os.path.exists(DBFILE):
            firsttime_flag = True

        self.__connect()
    
        if firsttime_flag: # this is run in the first place
            self.__create_default_tables()
            self.__create_default_variables()


    def refresh(self):
        self.__connect()

    def __connect(self):
        self.conn = sqlite3.connect(DBFILE)
        self.conn.execute("PRAGMA foreign_keys = OFF")
        self.conn.execute("PRAGMA synchronous = OFF")
        self.conn.execute("PRAGMA cache_size= 0") # N * 1024 -102400
        self.conn.execute("PRAGMA page_size = 50000")
        self.conn.execute("PRAGMA temp_store=2")
        self.conn.execute("PRAGMA journal_mode = OFF") #OFF

        self.conn.row_factory = sqlite3.Row
        self.cursor = self.conn.cursor()        


    def __create_default_tables(self):
        '''creates tables/indexes if they don\'t exist in the database'''
        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS dex_sources(
            id INTEGER NOT NULL PRIMARY KEY,
            shortname CHAR(15),
            name CHAR(100),
            author CHAR(50),
            publisher CHAR(50),
            year CHAR(64)
            )
        ''')
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_ds_id ON dex_sources(id)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_ds_sn ON dex_sources(shortname)")

        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS dex_inflections(
            id INTEGER NOT NULL PRIMARY KEY,
            descriptions CHAR(100)
            )
        ''')
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_di_id ON dex_inflections(id)")

        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS dex_lexems(
            id INTEGER NOT NULL PRIMARY KEY,
            timestamp INTEGER,
            form CHAR(100),
            description CHAR(100),
            accpos INTEGER
            )
        ''')
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_dl_id ON dex_lexems(id)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_dl_form ON dex_lexems(form)")

        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS dex_lexems_inflections(
            lexem_id INTEGER NOT NULL REFERENCES dex_lexems(id) ON DELETE RESTRICT,
            iid INTEGER NOT NULL REFERENCES dex_inflections(id) ON DELETE CASCADE,
            iform_dia CHAR(100),
            iform_simple CHAR(100)
            )
        ''')
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_li_lid ON dex_lexems_inflections(lexem_id)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_li_iid ON dex_lexems_inflections(iid)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_li_fdia ON dex_lexems_inflections(iform_dia)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_li_fsim ON dex_lexems_inflections(iform_simple)")

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS dex_definitions_extra(
                id INTEGER NOT NULL PRIMARY KEY,
                timestamp INTEGER,
                username CHAR(50),
                source_id INTEGER NOT NULL REFERENCES dex_sources(id) ON DELETE RESTRICT
            )
        ''')
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_dd_id ON dex_definitions_extra(id)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_dd_sid ON dex_definitions_extra(source_id)")

        self.cursor.execute('''
            CREATE VIRTUAL TABLE dex_definitions_ft USING FTS4(text)
        ''')

        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS dex_definitions_lexems(
            defid INTEGER NOT NULL REFERENCES dex_definitions(id) ON DELETE CASCADE,
            lexemid INTEGER NOT NULL REFERENCES dex_lexems(id) ON DELETE RESTRICT,
            PRIMARY KEY (defid, lexemid)
        )
        ''')

        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_ddl_id ON dex_definitions_lexems(defid)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_ddl_lid ON dex_definitions_lexems(lexemid)")


        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS dex_general(
              id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
              varname CHAR(50),
              varvalue CHAR(100)
        )
        ''')


        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS dex_abbrev_sources(
              id INTEGER NOT NULL,
              section CHAR(50)
        )
        ''')
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_das_id ON dex_abbrev_sources(id)")


        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS dex_abbrev_sections(
              section_name CHAR(50),
              abbrev_short CHAR(30),
              abbrev_content CHAR(70),
              abbrev_ambigous INTEGER DEFAULT 0
        )
        ''')
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_dae_sn ON dex_abbrev_sections(section_name)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_dae_sh ON dex_abbrev_sections(abbrev_short)")
        self.cursor.execute("CREATE INDEX IF NOT EXISTS idx_dae_am ON dex_abbrev_sections(abbrev_ambigous)")

        self.conn.commit()


    def __create_default_variables(self):
        '''these are hardcoded variables needed only when the db is created new'''
        try:
            variables_list = [
                ('lastupdate', '0'),
                ('update_link', 'http://dexonline.ro/update4.php?last=')
                ]
            self.cursor.executemany("REPLACE INTO dex_general(varname,varvalue) VALUES(?, ?)", variables_list)
            self.conn.commit()
        except Exception,e:
            print e
            self.conn.rollback()


# -------------------------------------------------------------------------------------------------
# DB Utils
# -------------------------------------------------------------------------------------------------
class DBUtils(DBBase):
    '''works with db'''
    firsttime_flag = False;
    outwin = None

    def __init__(self):
        DBBase.__init__(self)


    def get_sources(self):
        '''retrieves the sources as an array from the table'''
        try:
            self.cursor.execute("SELECT * FROM dex_sources WHERE 1")
            return self.cursor.fetchall()
        except Exception, e:
            print e


    def get_abbrevs(self):
        '''retrieves the abbreviations as an array from the table'''
        try:
            # for now, only dex_abbrev_sections
            self.cursor.execute("SELECT * FROM dex_abbrev_sections WHERE 1")
            return self.cursor.fetchall()
        except Exception, e:
            print e


    def get_settings(self):
        '''retrieves the settings variables from the table'''
        try:
            self.cursor.execute("SELECT * FROM dex_general WHERE 1")
            return self.cursor.fetchall()
        except Exception, e:
            print e




class DBSearchBase(DBBase):
    '''base class for searches'''
    results = "" # here will stay the results before parsing

    def __init__(self):
        DBBase.__init__(self)
        # use rowid for virtual tables in joins!
        self.gen_query = '''
            SELECT * FROM dex_lexems_inflections a
            JOIN dex_lexems b ON b.id = a.lexem_id
            JOIN dex_definitions_lexems c ON c.lexemid = b.id
            JOIN dex_definitions_extra d ON d.id = c.defid
            JOIN dex_sources f ON f.id = d.source_id
            JOIN dex_definitions_ft e ON e.rowid = d.rowid
            WHERE 1
        '''

    def parse_results(self):
        if not self.results:
               return ""

        formdict = {}
        for item in self.results:
            form = item['form']
            source = item['shortname']
            timestamp = item['timestamp']
            text = item['text']

            if form in formdict:
                formdict[form].append([source,  timestamp,  text])
            else:
                sublist = []
                sublist.append([source,  timestamp,  text])
                formdict[form] = sublist

        return formdict


    def set_source_id(self, source_id):
        self.source_id = source_id

        # source id (dictionaries) 0 - means all
        if source_id:
            self.gen_query = self.gen_query + " AND f.id = ? "


    @abstractmethod
    def do_search(self,  search_term): pass

    @abstractmethod
    def build_in_addition_to_general_query(self): pass


class DBSearchLexems(DBSearchBase):
    '''regular search for lexems'''
    with_diacritics = False
    exact = False

    def __init__(self):
        DBSearchBase.__init__(self)

    def do_search(self,  search_term):
        try:
            self.build_in_addition_to_general_query()

            if not self.exact:
                search_term = search_term + "%"  # prepare it for LIKE

            if self.source_id:
                search_elem = (self.source_id,  search_term)
            else:
                search_elem = (search_term,)

            self.cursor.execute(self.gen_query,  search_elem)
            # print self.gen_query # debug only
            self.results = self.cursor.fetchall()
        except Exception,e:
            print e
            return False


    def build_in_addition_to_general_query(self):
        ''' gen_query is imported from the parent class and we build on it'''
        q = self.gen_query

        # makes a difference between iform_dia/iform_simple from dex_lexems_inflections table
        if self.with_diacritics:
            #q = q + "AND a.iform_dia "
            q = q + "AND b.form "
        else:
            q = q + "AND a.iform_simple "


        # exact/contains type of search - selected in checkboxes (default: True)
        # makes a difference in sql query ( =  or LIKE)
        if self.exact:
            q = q + " = ? "
        else:
            q = q + " LIKE ? "

        q = q + " GROUP BY defid ORDER BY form "

        self.gen_query = q



class DBSearchDefinitions(DBSearchBase):
    '''fulltext search on definitions'''
    def __init__(self):
        DBSearchBase.__init__(self)

    def do_search(self,  search_term):
        try:
            self.build_in_addition_to_general_query()
            self.cursor.execute(self.gen_query,  (search_term,  ))
            self.results = self.cursor.fetchall()
        except Exception,e:
            print e
            return False

    def build_in_addition_to_general_query(self):
        ''' gen_query is imported from the parent class and we build on it'''
        self.gen_query += " AND e.text MATCH ? GROUP BY defid ORDER BY form"
        # print self.gen_query #debug
