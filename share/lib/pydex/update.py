# -*- coding: utf-8 -*-
#
#       update.py
#
#       Copyright Cristian Navalici ncristian [at]  lemonsoftware.eu
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import os
import sys
import wx
import httplib2
import unicodedata
import time
import sqlite3

from lxml import etree
from gzip import GzipFile
from os.path import getsize
from tempfile import NamedTemporaryFile
from urllib2 import Request, build_opener
from abc import abstractmethod
from itertools import tee
#from pympler.asizeof import asizeof # debug only
#from pympler import tracker, summary, muppy
from collections import deque

from settings import SettingsDB
from pydex import TMP_FOLDER
from db_utils import DBBase
from pydex_dialogs import NewTextEvent, TextEventType
from helpers import  convert_term_correct_utf8


class UpdateDB(object):
    def __init__(self, communicator = None):
        sob = SettingsDB()
        self.update_url = sob.get_update_link() + sob.get_lastupdate()
        if communicator: self.outwin = communicator


    def run(self):
        '''main procedure - control center (return False if fails)'''
        try:
            # step 1: get the file with the links to be processed
            self.xmlcontent = self.__grab_xml_with_links()

            # step 2: update each section as we know it
            self.__run(SourcesSection(), "Surse")
            self.__run(AbbrevsSection(), "Abrevieri")
            self.__run(InflectionsSection(), "Inflexiuni")
            self.__run(DefinitionsSection(), "Definiţii")
            self.__run(LexemsSection(), "Lexeme")
            self.__run(LexDefAssocSection(), "Asociaţii Lexeme-Definiţii")

            # step 3: update "last date"
            self.__run(LastDateSection(), "dată")

            return True
        except Exception as e:
            dialog = wx.MessageDialog(None, "{}".format(e), "EROARE:", wx.OK | wx.ICON_WARNING)
            dialog.ShowModal()
            dialog.Destroy()
            return False


    def abort(self): return

    def __run(self, secob, msg = ''):
        '''internal run - not intended to be used directly'''
        self.__communicate_with_gui("Actualizare {}".format(msg))
        secob.set_communicator(self.outwin)
        secob.xml_with_links = self.xmlcontent
        secob.run()
        del(secob)


    def __communicate_with_gui(self,  msg = ''):
        "used to comunicate with gui by custom events"
        if self.outwin is None: return

        evt = NewTextEvent(TextEventType, -1)
        evt.setText(msg)
        wx.PostEvent(self.outwin, evt)


    def __grab_xml_with_links(self):
        '''grab the initial xml file with links to be processed
        @see: http://wiki.dexonline.ro/wiki/Update4Instructions'''
        try:
            url = self.update_url
            #url = "http://dexonline.ro/update4.php?last=2011-01-18" # debug only
            string_content = self.__create_opener_httplib(url)[1]

            return etree.XML(string_content)
        except Exception as e:
            raise Exception("A apărut o problemă la descărcarea fişierului cu informaţiile actualizării {}".format(e))


    def __create_opener_httplib(self, url):
        try:
            h = httplib2.Http(".cache")
            resp, content = h.request(url, "GET")
            return (resp, content)
        except Exception as e:
            raise Exception("A apărut o problemă la crearea conexiunii {}".format(e))



# -------------------------------------------------------------------------------------------------
# SECTION BASE CLASS
# -------------------------------------------------------------------------------------------------
class SectionBase(object):
    xml_with_links = "" # Etree Element
    conn = None
    cursor = None
    outwin = None
    section_code = None

    def __init__(self):
        dob = DBBase()
        self.conn = dob.conn
        self.cursor = dob.cursor


    @abstractmethod
    def run(self): return

    @abstractmethod
    def parse_save_xml(self): return


    def common_run(self):
        links = self.extract_links_for_section(self.section_code)
        if not links: return

        for link in links:
            self.filecontent = self.download_file(link)
            self.parse_save_xml()


    def set_communicator(self, commDlg):
        '''used in derived classes to communicate with the GUI Dialog'''
        if commDlg: self.outwin = commDlg


    def communicate_with_gui(self,  msg = ''):
        "used to comunicate with gui by custom events"
        if self.outwin is None: return

        evt = NewTextEvent(TextEventType, -1)
        evt.setText(msg)
        wx.PostEvent(self.outwin, evt)



    def download_file(self, url):
        ''' download the specified gzipped file and return and file object of it'''
        # downloading
        tempgzfile = self.create_temp_file('.gz')
        r = self.__create_opener(url)

        length = r.headers["Content-length"]

        size = 0
        while True:
            a = r.read(1024*1024)
            if not a: break
            tempgzfile.write(a)

            size += len(a)
            self.communicate_with_gui("Descărcare {}: {} / {} bytes".format(self.section_name, size, length))

        tempgzfile.seek(0) # important for tempfile!

        # extracting gzipped content
        gzipper = GzipFile(tempgzfile.name)
        gzipper_read = 0

        tempxmlfile = self.create_temp_file('.xml')

        size = 1048576 * 10
        while 1:
            content = gzipper.read(size)
            gzipper_read += size
            # print count, len(content) # debug
            if not content:
                gzipper = None
                break
            tempxmlfile.write(content)

        tempxmlfile.seek(0) # important for tempfile!
        #return etree.iterparse(tempxmlfile) # don't use here, the iterator is created later
        return tempxmlfile



    def extract_links_for_section(self, sectionName = ''):
        '''parse the initial xml file and get the links to be processed
        @see: http://wiki.dexonline.ro/wiki/Update4Instructions'''
        if sectionName not in ['Abbrevs', 'Inflections', 'Sources', 'Definitions', 'Lexems', 'LexemDefinitionMap']:
            raise Exception("Secţiune necunoscută: {}".format(sectionName))

        if self.xml_with_links is None: raise Exception("Fişierul XML cu legături este gol şi nu ar fi trebuit să fie.")

        results = []

        # check first the full section
        full_section = self.xml_with_links.find("Full")
        fres = full_section.findtext(sectionName)
        if fres: results.append(fres)

        # check then the partial section
        partial_section = self.xml_with_links.find("Diffs")
        for p in partial_section.findall("Diff"):
            pres = p.findtext(sectionName)
            if pres: results.append(pres)
        
        # print results # debug
        return results


    def calculate_size(self, iterator):
        '''only to calculate the size of the interator'''
        try:
            return sum(1 for _ in iterator)
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            pass
        




    def create_temp_file(self, extension='.gz', delete_flag = True):
        '''creates a temp file to be used for major grabbing'''
        try:
            if not os.path.exists(TMP_FOLDER): os.mkdir(TMP_FOLDER)
            return NamedTemporaryFile(mode="a+b", dir=TMP_FOLDER, prefix="pydex_", suffix=extension, delete=delete_flag)
        except:
            raise Exception("Fişierul temporar nu a putut fi creat în {}".format(TMP_FOLDER))


    def __create_opener(self, url):
        '''common part for grabbing methods'''
        try:
            request = Request(url)
            request.add_header('Accept-encoding', 'gzip')
            request.add_header('Keep-alive', 'true')
            opener = build_opener()
            return opener.open(request)
        except:
            raise Exception("A apărut o problemă la conectarea la server")



    def __create_opener_httplib(self, url):
        '''not used due the lack of a progress callback function'''
        h = httplib2.Http(".cache")
        resp, content = h.request(url, "GET")
        return (resp, content)






# DERIVED CLASSES FROM SECTION BASE CLASS

# -------------------------------------------------------------------------------------------------
# SOURCES
# -------------------------------------------------------------------------------------------------
class SourcesSection(SectionBase):
    section_name = "Surse"
    section_code = "Sources"
    content_sources = None


    def run(self): self.common_run()

    def parse_save_xml(self):
        try:
            '''parse and also save to db the results'''
            if self.filecontent is None: raise Exception("Nothing to be parsed")
            
            self.content_sources = etree.iterparse(self.filecontent.name, tag="Source", encoding="UTF8")
            if self.content_sources is None: return
            
            query = "INSERT INTO dex_sources VALUES (?,?,?,?,?,?)"
            self.cursor.execute("DELETE FROM dex_sources")

            self.conn.executemany(query, self.sources_generator())
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            raise Exception("[{}]: Eroare la salvare în baza de date {}".format(self.section_name, e))


    def sources_generator(self):
        try:
            for ev, sourceElem in self.content_sources:
                idElem = int(sourceElem.get("id"))
                snElem = sourceElem.findtext("ShortName", "")
                naElem = sourceElem.findtext("Name", "")
                auElem = sourceElem.findtext("Author", "")
                puElem = sourceElem.findtext("Publisher", "")
                yeElem = sourceElem.findtext("Year", "")
                yield (idElem,snElem,naElem,auElem,puElem,yeElem)
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            pass        


# -------------------------------------------------------------------------------------------------
# ABBREVIATIONS
# -------------------------------------------------------------------------------------------------
class AbbrevsSection(SectionBase):
    section_name = "Abrevieri"
    section_code = "Abbrevs"
    content_source = None
    content_section = None

    def run(self): self.common_run() 


    def parse_save_xml(self):
        try:
            '''parse and also save to db the results'''
            if self.filecontent is None: raise Exception("Nothing to be parsed")

            self.content_source = etree.iterparse(self.filecontent.name, tag="Source", encoding="UTF8")
            self.content_section = etree.iterparse(self.filecontent.name, tag="Section", encoding="UTF8")
            if self.content_source is None or self.content_section is None: return

            # STEP0: full update so clean up first the tables
            self.cursor.execute("DELETE FROM dex_abbrev_sources")
            self.cursor.execute("DELETE FROM dex_abbrev_sections")

            # STEP1: first the sources section
            query_1 = "INSERT INTO dex_abbrev_sources VALUES (?,?)"
            self.conn.executemany(query_1, self.ab_sources_generator())

            # STEP2: here the section elements with abbreviations
            query_2 = "INSERT INTO dex_abbrev_sections VALUES (?,?,?,?)"
            self.conn.executemany(query_2, self.ab_sections_generator())

            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            raise Exception("[{}]:Eroare la salvare în baza de date {}".format(self.section_name, e))


    def ab_sources_generator(self):
        try:
            for ev, sourceElem in self.content_source:
                sid = sourceElem.get('id')
                sections = sourceElem.findall("Section")
                if not sections: return
                for sec in sections:
                    yield([sid, sec.text])
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            pass          


    def ab_sections_generator(self):
        try:
            for ev, sectionElem in self.content_section:
                nameSec = sectionElem.get('name')
                # because we have Sections also in Sources part, check for name!
                if nameSec:
                    abbrev = sectionElem.findall("Abbrev")
                    if not abbrev: return
                    for ab in abbrev:
                        short = ab.get("short")
                        ambiguous = ab.get("ambiguous", 0)
                        content = ab.text
                        yield([nameSec, short, content, ambiguous])
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            pass  


# -------------------------------------------------------------------------------------------------
# INFLECTIONS
# -------------------------------------------------------------------------------------------------
class InflectionsSection(SectionBase):
    section_name = "Inflexiuni"
    section_code = "Inflections"
    content_inflections = None

    def run(self): self.common_run()


    def parse_save_xml(self):
        try:
            '''parse and also save to db the results'''
            if self.filecontent is None: raise Exception("Nothing to be parsed")

            self.content_inflections = etree.iterparse(self.filecontent.name, tag="Inflection", encoding="UTF8")
            if self.content_inflections is None: return

            self.cursor.execute("DELETE FROM dex_inflections")
            query = "INSERT INTO dex_inflections VALUES (?,?)"
            self.conn.executemany(query, self.inflections_generator())
            
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            raise Exception("[{}]:Eroare la salvare în baza de date {}".format(self.section_name, e))


    def inflections_generator(self):
        try:
            for ev, inflecElem in self.content_inflections:
                idElem = inflecElem.get("id")
                deElem =  inflecElem.findtext("Description")
                yield([idElem,deElem])
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            pass  





# -------------------------------------------------------------------------------------------------
# DEFINITIONS
# -------------------------------------------------------------------------------------------------
class DefinitionsSection(SectionBase):
    section_name = "Definiţii"
    section_code = "Definitions"
    content_definitions = None
    chunk_size = 5000

    def __init__(self):
        super(DefinitionsSection, self).__init__()
        self.definition_counter = 0
        self.definitions_list = deque()


    def run(self): self.common_run()


    def parse_save_xml(self):
        '''parse and also save to db the results'''
        if self.filecontent is None: raise Exception("Nothing to be parsed")
        
        #Xname = '/tmp/dexonline/2013-03-16-definitions.xml'
        self.content_definitions = etree.iterparse(self.filecontent.name, tag="Definition")
        if self.content_definitions is None: raise Exception("Nothing to be parsed")

        small_counter = 0

        try:
            while True:
                nextelem = self.definitions_generator().next()

                if nextelem is None:
                    break
                
                if nextelem[0] is True:
                    self.__remove_definition(nextelem[1])
                else:
                    self.definitions_list.append(nextelem[1:])
                    small_counter = small_counter + 1
        
                del nextelem
                
                if (small_counter == self.chunk_size):
                    self.__save_definitions()
                    self.definitions_list.clear()                
                    small_counter = 0
        except StopIteration:
            # intercept the end of iteration (because of the next())
            pass                    
        except Exception as e:
            raise Exception("[{}]:Eroare {}".format(self.section_name, e))
          
        

    def definitions_generator(self):
        try:
            for ev, defElem in self.content_definitions:
                idElem = defElem.get("id")
                timestampElem = defElem.findtext("Timestamp")
                deletedFlag = defElem.find("Deleted", None)
                usernameElem = defElem.findtext("UserName", '')
                sourceidElem = defElem.findtext("SourceId", 0)
                textElem = defElem.findtext("Text", '')
                
                defElem.clear()
                while defElem.getprevious() is not None: del defElem.getparent()[0]

                yield([deletedFlag, idElem, timestampElem, usernameElem, sourceidElem, textElem])
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            print e
            pass

            


    def __save_definitions(self):
        '''_'''
        try:
            self.definition_counter += len(self.definitions_list)
            self.communicate_with_gui("Total definiţii salvate...{}".format(self.definition_counter))

            
            for d in self.definitions_list:
                self.cursor.execute("REPLACE INTO dex_definitions_extra VALUES  (?,?,?,?)", d[:-1])
                self.cursor.execute("REPLACE INTO dex_definitions_ft(rowid,text) VALUES(last_insert_rowid(), ?)", d[-1:])

            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            raise Exception("[{}]:Eroare la salvare în baza de date {}".format(self.section_name, e))



    def __remove_definition(self, idElem):
        '''removes a definition by id'''
        try:
            self.cursor.execute("DELETE FROM dex_definitions_extra WHERE id = ?", (idElem, ))
            self.conn.commit()
        except Exception, e:
            raise Exception("[{}]:Eroare la ştergerea unei înregistrări {}".format(self.section_name, e))
            self.conn.rollback()





# -------------------------------------------------------------------------------------------------
# LEXEMS
# -------------------------------------------------------------------------------------------------
class LexemsSection(SectionBase):
    section_name = "Lexeme"
    section_code = "Lexems"

    def run(self): self.common_run()

    def parse_save_xml(self):
        '''parse and also save to db the results'''
        if self.filecontent is None: raise Exception("Nothing to be parsed")
        self.content_lexems = etree.iterparse(self.filecontent.name, tag="Lexem")

        if self.content_lexems is None: return

        try:
            counter = 0
            for p in self.lexems_generator():
                self.cursor.execute("REPLACE INTO dex_lexems VALUES (?,?,?,?,?)", p[0])
                self.cursor.executemany("INSERT INTO dex_lexems_inflections VALUES (?,?,?,?)", p[1])
                counter = counter + 1
                self.communicate_with_gui("Salvare lexeme...{}".format(counter)) 
            self.conn.commit()

        except Exception as e:
            self.conn.rollback()
            raise Exception("[{}]:Eroare la salvare în baza de date {}".format(self.section_name, e))



    def lexems_generator(self):
        try:
            for ev, lexemElem in self.content_lexems:
                idElem = lexemElem.get("id")
                timestampElem = lexemElem.findtext("Timestamp")

                raw_formElem = lexemElem.findtext("Form")
                formElem = raw_formElem.replace("'", "") # remove the accent when saving into the table
                formElem = convert_term_correct_utf8(formElem)
                descElem = lexemElem.findtext("Description", "")
                
                accpos = raw_formElem.find("'")        # but get its position (-1 if it's not found)
                if accpos is None: accpos = -1

                # from wiki: "Lexem has always at least ONE inflection"
                inflections = deque()                
                for inflecl in lexemElem.findall("InflectedForm"):
                    iid = inflecl.findtext("InflectionId")
                    parsed_form = self.__parse_received_form(inflecl.findtext("Form")) # (original form, wout diacritics form) both wout single quote
                    inflections.append([idElem, iid, parsed_form[0], parsed_form[1]])
                    inflecl.clear()
                
                lexemElem.clear()
                yield([idElem, timestampElem, formElem, descElem, accpos], inflections)
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            pass  
    
    

    def __parse_received_form(self, formreceived):
        '''transforms a romanian form (with special chars) into ASCII form'''
        try:
            # we need here two things: the original string wout ' (simple quote)
            # and the string wout ' and wout diacritics
            form1 = formreceived.replace("'", "")

            if isinstance(formreceived, str):
                input =  unicode(form1, 'UTF-8')
                #input = form.decode('utf-8') # equivalent to  unicode(form, 'UTF-8')
                                # for python >= 3.0 just use form wout decode
            else:
                input = form1
            norm = unicodedata.normalize('NFKD', input).encode('ASCII', 'ignore')
            return (form1, norm)
        except Exception as e:
            raise Exception("[{}]:Eroare la transformare formă {}".format(self.section_name, e))




# -------------------------------------------------------------------------------------------------
# LEXEMS DEFINITIONS ASSOCIATIONS
# -------------------------------------------------------------------------------------------------
class LexDefAssocSection(SectionBase):
    section_name = "Asociaţii Lexeme-Definiţii"
    section_code = "LexemDefinitionMap"

    def __init__(self):
        super(LexDefAssocSection, self).__init__()
        self.lexdef_list = deque()

    def run(self): self.common_run()

    def parse_save_xml(self):
        '''parse and also save to db the results'''
        try:
            if self.filecontent is None: raise Exception("Nothing to be parsed")

            self.content_lexdef = etree.iterparse(self.filecontent.name, tag="Map")
            if self.content_lexdef is None: return

            self.conn.executemany("REPLACE INTO dex_definitions_lexems(defid, lexemid) VALUES (?,?)", self.deflex_generator())
            
            self.u_content =  etree.iterparse(self.filecontent.name, tag="Unmap")
            for umap_elem in self.unmap_generator():
                self.__remove_lexdef_association(umap_elem)
            
            self.conn.commit()
        except Exception as e:
            raise Exception("[{}]:Eroare la salvare în baza de date {}".format(self.section_name, e))
        

    def deflex_generator(self):
        try:
            for ev, ldElem in self.content_lexdef:
                lexemId = ldElem.get("lexemId")
                defId = ldElem.get("definitionId")
                ldElem.clear()  #free some memory. very important!
                yield(defId, lexemId)
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            pass  


    def unmap_generator(self):
        try:
            for ev, u_map in self.u_content:
                defId = u_map.get("definitionId")
                lexemId = u_map.get("lexemId")
                u_map.clear()
                yield([defId, lexemId])
        except etree.XMLSyntaxError as e:
            # it raises an XMLSyntaxError even if there's none (bug?)
            pass  



    def __remove_lexdef_association(self, umap_elem):
        try:
            defid, lexemid = umap_elem
            self.communicate_with_gui("Ştergere asociaţie:{} pentru lexem:{}".format(defid, lexemid))
            self.cursor.execute("DELETE FROM dex_definitions_lexems WHERE defid = ? AND lexemid = ?", (defid, lexemid))
            
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            raise Exception("[{}]:Eroare la ştergerea unei înregistrări {}".format(self.section_name, e))




# -------------------------------------------------------------------------------------------------
# LAST DATE UPDATE
# -------------------------------------------------------------------------------------------------
class LastDateSection(SectionBase):
    section_name = "Full"

    def run(self):
        update_date = self.xml_with_links.find("Full").get("date")
        self.__save_update_date(update_date)


    def __save_update_date(self, update_date_raw):
        try:
            update_date = self.__check_date(update_date_raw)
            self.cursor.execute("UPDATE dex_general SET varvalue = ? WHERE varname ='lastupdate'", (update_date,))
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            raise Exception("[{}]:Eroare la salvare în baza de date {}".format(self.section_name, e))


    def __check_date(self, update_date_raw):
        try:
            time.strptime(update_date_raw, "%Y-%m-%d")
            return update_date_raw
        except ValueError:
            raise Exception("Data ultimei actualizări este invalidă: {}".format(update_date_raw))
