# -*- coding: utf-8 -*-

from os.path import abspath, dirname, join, pardir
import os
import sys
import sqlite3

SHARED_FILES = abspath(join(dirname(__file__), pardir, pardir))
TMP_FOLDER = abspath(join(SHARED_FILES,"tmp"))
DBPATH = join(SHARED_FILES, 'data/')
DBFILE = join(DBPATH, 'pydex.db')
ICONFILE = join(DBPATH,  'pydex.ico')
PYDEX_VERSION = '1.2'
PYDEX_NAME = 'PyDEX'
HTML_HEADER = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">'

ABOUT_DICT = {
    'description': 'PyDEX este un utilitar ce vă permite accesarea offline a bazei de date dexonline.ro, dar şi actualizarea acesteia direct de pe site.',
    'licence': 'PyDEX este un program cu surse libere. Este guvernat de licenţa GPLv3\nVerificaţi fişierul LICENSE pentru licenţă pe larg.',
    'copyright': '© 2010 - 2013 Cristian Năvălici',
    'website': 'http://pydex.lemonsoftware.eu',
    'developer': 'Cristian Năvălici\nMulţumiri pentru suport echipei dexonline \n şi lui Cătălin Frâncu în mod deosebit.',
    'docwriter': 'Cristian Năvălici'
}

# creates the link with bin/pydex <-> LINE: from pydex import main
from pydex.main import main
