# -*- coding: utf-8 -*-
#
#       controller.py
#
#       Copyright Cristian Navalici ncristian [at]  lemonsoftware.eu
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import wx

from db_utils import DBSearchDefinitions, DBUtils, DBSearchLexems
from settings import SettingsDB
from update import UpdateDB
from pydex_dialogs import DbUpdateDlg, SourcesDlg, AbbrevsDlg
from helpers import SearchManager, html_format_source, html_format_string
from pydex import HTML_HEADER


class Controller(object):
    '''this is the only class that works directly with main.py'''
    search_results = []


    def __init__(self, dexframe_obj):
        self.dbutils = DBUtils()
        self.settings = SettingsDB()
        self.search_mng = SearchManager()
        self.dexframe_obj = dexframe_obj

    def get_db_last_update(self):
        lupd = self.settings.get_lastupdate(forceupdate = 1)

        if lupd == "0": return 'Niciodată'
        else: return lupd


    def show_update(self, parent_window):
        dgl = DbUpdateDlg(parent_window)
        dgl.ShowModal()
        parent_window.update_statusbar_lastupdate()


    def show_sources(self, parent_window):
        '''show sources'''
        sources = self.dbutils.get_sources()
        if not sources:
            dlg = wx.MessageDialog(None, "Tabela de surse este goală, nimic de afişat",
                                   "INFO:", wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
        else:
            dgl = SourcesDlg(parent_window, sources)
            dgl.Show()


    def show_abbrevs(self, parent_window):
        '''show abbreviations'''
        abbrevs = self.dbutils.get_abbrevs()
        if not abbrevs:
            dlg = wx.MessageDialog(None, "Tabela de abrevieri este goală, nimic de afişat",
                                   "INFO:", wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
        else:
            dgl = AbbrevsDlg(parent_window, abbrevs)
            dgl.Show()


    def do_search(self, search_options):
        search_term = search_options['search_term']

        if search_options['search_type'] == 'definitions':
            sob = DBSearchDefinitions()

        if search_options['search_type'] == 'lexems':
            sob = DBSearchLexems()
            sob.with_diacritics = search_options['options']['diacritics']
            sob.exact = search_options['options']['exact']

        sob.set_source_id(search_options['source_id'])
        sob.do_search(search_term)

        self.search_mng.notify_search(search_term)

        self.search_results = sob.parse_results()
        return self.search_results


    def do_db_update(self, dialogWindow):
        '''it does a database update - doesnt matter if full or partial'''
        aob = UpdateDB(communicator = dialogWindow)
        return aob.run()


    def populate_dictionaries_sources(self):
        sourcesArr = ["Toate"]
        sourcesArr.extend(self.get_db_sources(shortname = True))
        return sourcesArr


    def get_db_sources(self, shortname = False):
        sources = self.dbutils.get_sources()
        if not sources: return []

        if shortname:
            results = []
            [ results.append(s[1]) for s in sources ]
            return results

        return sources


    def get_search_history(self):
        return self.search_mng.history_search


    def update_results_text_area(self):
        self.dexframe_obj.txtmain.SetPage('')
        self.string_display = ''

        if not self.search_results:
            self.dexframe_obj.txtmain.SetPage("Fără rezultate...")
        else:
            # this will be used in printing to maintain formatting
            self.string_display = HTML_HEADER

            results_counter = 1
            for lexem, content in self.search_results.iteritems():
                title_lexem = "<h3>"+lexem+"</h3>"

                self.dexframe_obj.txtmain.AppendToPage(title_lexem) # don't use format, it throws UnicodeEncodeError
                self.string_display += title_lexem

                for c in content:
                    results_counter += 1
                    self.dexframe_obj.update_statusbar_results(results_counter)

                    content = html_format_string(c[2])
                    source = html_format_source(c[0], c[1])

                    full_content_per_item = content + source + '<br>'
                    self.dexframe_obj.txtmain.AppendToPage(full_content_per_item)

                    self.string_display += full_content_per_item
                    wx.Yield()

